const express=require('express')
const router=express.Router()
const userControllers=require('../controllers/userControllers')
const auth=require('../auth')

// no params
router.post('/register',userControllers.registration)
router.post('/login',userControllers.login)
router.get('/profile',userControllers.userDetails)
// with params
router.patch('/updateRole/:userId',auth.verify,userControllers.updateRole)
router.post('/addToCart/:productId',auth.verify,userControllers.addToCart)

module.exports=router