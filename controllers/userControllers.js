// dependencies
const User=require('../models/User')
const bcrypt=require('bcrypt')
const auth=require('../auth')
const Products = require('../models/Products')

// registration and availability
module.exports.registration=(req,res)=>{
    const newUser = new User(
        {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password,10),
            mobileNo: req.body.mobileNo,
            cartQuantity: req.body.cartQuantity
        })
    return User.find({email:req.body.email})
    .then(result=>{
        if(result.length>0){
            return res.send(`Sorry beh, taken na sya. Move on na.`)
        }else{
            return newUser.save()
            .then(user=>{
                console.log(newUser);
                res.send(`Registered yarn!`)
            })
            .catch(err=>{
                res.send(`Try lang ng try. Fighting girl!`)
            })
        }})
    .catch(err=>{
        res.send(err)
    })
}

// login
module.exports.login=(req,res)=>{
    return User.findOne({email:req.body.email})
    .then(result=>{
        if(!result){
            return res.send(`Register ka muna.`)
        }else{
            const checkPassword=bcrypt.compareSync(req.body.password, result.password);
            if(checkPassword){
                return res.send({accessToken: auth.createToken(result)})
            }else{
                return res.send('password incorrect')
            }}
    })
}

// update a role
module.exports.updateRole=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const idUpdate=req.params.userId;
    if(userData.isAdmin){
        return User.findById(idUpdate)
        .then(result=>{
            const update={isAdmin: !result.isAdmin}
            return User.findByIdAndUpdate(idUpdate,update,{new:true})
            .then(document=>{
                document.password="******"
                return res.send(document)
            })
            .catch(err=>{res.send(err)})
        })
        .catch(err=>{res.send(err)})
    }else{
        return res.send('Hindi ka po admin')
    }
}

// retrieve user details
module.exports.userDetails=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    return User.findById(userData.id)
    .then(result=>{
        result.password="******"
        return res.send(result)
    })
    .catch(err=>res.send(err))
}

// add to cart
module.exports.addToCart=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        const productId=req.params.productId;
        const data={
            productId:productId,
            userId:userData.id
        }
        const stockUpdate=await Products.findById(productId)
        .then(result=>{
            result.itemsInCart.push({userId:data.userId})
            result.stocks--;
            return result.save()
            .then(success=>true)
            .catch(err=>false)
        })
        .catch(err=>false)

        const userUpdate=await User.findById(data.userId)
        .then(result=>{
            result.items.push({productId:data.productId})
            result.cartQuantity++;
            return result.save()
            .then(success=>true)
            .catch(err=>false)
        })
        .catch(err=>false)
        console.log(userUpdate)
        console.log(stockUpdate)
        return (stockUpdate && userUpdate) ? res.send(true) : res.send(false)
    }else{
        return res.send(`Paninda mo, bili mo?`)
    }
}