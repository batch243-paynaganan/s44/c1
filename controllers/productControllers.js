const Products=require('../models/Products')
const auth=require('../auth')

// add a course
module.exports.createProduct=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const newProduct=new Products({
        productName: req.body.productName,
        productDescription: req.body.productDescription,
        productPrice: req.body.productPrice,
        productColor: req.body.productColor,
        productSize: req.body.productSize,
        stocks: req.body.stocks
    })
    if(userData.isAdmin){
        newProduct.save()
        .then(result=>{
            console.log(result)
            res.send(`Product has been added`)
        })
        .catch(err=>{
            console.log(err)
            res.send(`Process interrupted`)
        })
    }else{
        res.send(`Hindi ka admin beh, wag assuming`)
    }
}

// retrieve active products
module.exports.activeProducts=(req,res)=>{
    return Products.find({isActive:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

// retrieve all products
module.exports.allProducts=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        return res.send('Hindi ka admin beh, wag assuming.')
    }else{
        return Products.find({})
        .then(result=>res.send(result))
        .catch(err=>res.send(err))
    }
}

// retrieve a single product
module.exports.getProduct=(req,res)=>{
    const productId=req.params.productId;
    return Products.findById(productId)
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

// update a product
module.exports.updateProduct=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const update={
        productName: req.body.productName,
        productDescription: req.body.productDescription,
        productPrice: req.body.productPrice,
        productColor: req.body.productColor,
        productSize: req.body.productSize
    }
    const productId=req.params.productId;
    if(userData.isAdmin){
        return Products.findByIdAndUpdate(productId,update,{new:true})
        .then(result=>res.send(result))
        .catch(err=>res.send(err))
    }else{res.send(`unauthorized update`)}
}

// archive a product
module.exports.archiveProduct=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const archive={isActive:req.body.isActive}
    const productId=req.params.productId
    if(userData.isAdmin){
        return Products.findByIdAndUpdate(productId,archive,{new:true})
        .then(result=>res.send(true))
        .catch(err=>res.send(false))
    }else{
        res.send(`Feeling admin yarn?`)
    }
}

